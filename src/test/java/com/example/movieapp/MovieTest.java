package com.example.movieapp;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MovieTest {

    @Test
    public void testIdGetterAndSetter() {
        // Arrange
        Movie movie = new Movie();
        UUID id = UUID.randomUUID();

        // Act and Assert
        movie.setId(id);
        assertEquals(id, movie.getId());
    }

    @Test
    public void testTitleGetterAndSetter() {
        // Arrange
        Movie movie = new Movie();
        String title = "Sample Movie";

        // Act and Assert
        movie.setTitle(title);
        assertEquals(title, movie.getTitle());
    }

    @Test
    public void testReleaseDateGetterAndSetter() {
        // Arrange
        Movie movie = new Movie();
        LocalDate releaseDate = LocalDate.of(2023, 10, 18);

        // Act and Assert
        movie.setReleaseDate(releaseDate);
        assertEquals(releaseDate, movie.getReleaseDate());
    }

    @Test
    public void testRuntimeGetterAndSetter() {
        // Arrange
        Movie movie = new Movie();
        int runtime = 120;

        // Act and Assert
        movie.setRuntime(runtime);
        assertEquals(runtime, movie.getRuntime());
    }

    @Test
    public void testActorsGetterAndSetter() {
        // Arrange
        Movie movie = new Movie();
        List<Actor> actors = new ArrayList<>();

        // Act and Assert
        movie.setActors(actors);
        assertEquals(actors, movie.getActors());
    }

    @Test
    public void testDirectorsGetterAndSetter() {
        // Arrange
        Movie movie = new Movie();
        List<Director> directors = new ArrayList<>();

        // Act and Assert
        movie.setDirectors(directors);
        assertEquals(directors, movie.getDirectors());
    }
}
