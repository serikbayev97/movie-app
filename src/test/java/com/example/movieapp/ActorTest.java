package com.example.movieapp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ActorTest {

    @Test
    public void testActorNameGetterAndSetter() {
        // Arrange
        Actor actor = new Actor();
        String name = "John Doe";

        // Act and Assert
        actor.setName(name);
        assertEquals(name, actor.getName());
    }

    @Test
    public void testActorAgeGetterAndSetter() {
        // Arrange
        Actor actor = new Actor();
        int age = 35;

        // Act and Assert
        actor.setAge(age);
        assertEquals(age, actor.getAge());
    }
}