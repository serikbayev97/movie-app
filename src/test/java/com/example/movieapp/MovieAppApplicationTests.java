package com.example.movieapp;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MovieAppApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    public void testMainMethod() {
        // You can simply call the main method and check for any exceptions.
        MovieAppApplication.main(new String[]{});

        // If the main method executes without exceptions, the test will pass.
        // You can also add additional checks here as needed.
    }

}
