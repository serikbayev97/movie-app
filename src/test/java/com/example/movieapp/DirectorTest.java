package com.example.movieapp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DirectorTest {

    @Test
    public void testDirectorNameGetterAndSetter() {
        // Arrange
        Director director = new Director();
        String name = "Jane Smith";

        // Act and Assert
        director.setName(name);
        assertEquals(name, director.getName());
    }

    @Test
    public void testDirectorAgeGetterAndSetter() {
        // Arrange
        Director director = new Director();
        int age = 45;

        // Act and Assert
        director.setAge(age);
        assertEquals(age, director.getAge());
    }
}
