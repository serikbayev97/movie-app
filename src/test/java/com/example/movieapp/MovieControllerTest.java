package com.example.movieapp;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MovieControllerTest {

    @Mock
    private MovieRepository movieRepository;

    @InjectMocks
    private MovieController movieController;

    @Test
    public void testGetAllMovies() {
        // Arrange
        List<Movie> movieList = Arrays.asList(new Movie(), new Movie());
        when(movieRepository.findAll()).thenReturn(movieList);

        // Act
        List<Movie> result = movieController.getAllMovies();

        // Assert
        assertEquals(movieList, result);
    }

    @Test
    public void testGetMovie() {
        // Arrange
        UUID movieId = UUID.randomUUID();
        Movie expectedMovie = new Movie();
        when(movieRepository.findById(movieId)).thenReturn(Optional.of(expectedMovie));

        // Act
        Movie result = movieController.getMovie(movieId);

        // Assert
        assertEquals(expectedMovie, result);
    }

    @Test
    public void testGetMovie_NotFound() {
        // Arrange
        UUID movieId = UUID.randomUUID();
        when(movieRepository.findById(movieId)).thenReturn(Optional.empty());

        // Act
        Movie result = movieController.getMovie(movieId);

        // Assert
        assertNull(result);
    }

    @Test
    public void testCreateMovie() {
        // Arrange
        Movie movieToCreate = new Movie();
        movieToCreate.setId(UUID.randomUUID());
        when(movieRepository.save(movieToCreate)).thenReturn(movieToCreate);

        // Act
        Movie result = movieController.createMovie(movieToCreate);

        // Assert
        assertEquals(movieToCreate, result);
    }

    @Test
    public void testUpdateMovie() {
        // Arrange
        UUID movieId = UUID.randomUUID();
        Movie movieToUpdate = new Movie();
        when(movieRepository.save(movieToUpdate)).thenReturn(movieToUpdate);

        // Act
        Movie result = movieController.updateMovie(movieId, movieToUpdate);

        // Assert
        assertEquals(movieToUpdate, result);
    }

    @Test
    public void testDeleteMovie() {
        // Arrange
        UUID movieId = UUID.randomUUID();

        // Act
        movieController.deleteMovie(movieId);

        // Assert
        verify(movieRepository, times(1)).deleteById(movieId);
    }

    @Test
    public void testGetMoviesByTitle() {
        // Arrange
        String title = "Sample Movie";
        List<Movie> movieList = Arrays.asList(new Movie(), new Movie());
        when(movieRepository.findByTitle(title)).thenReturn(movieList);

        // Act
        List<Movie> result = movieController.getMoviesByTitle(title);

        // Assert
        assertEquals(movieList, result);
    }

    @Test
    public void testGetMoviesByReleaseDate() {
        // Arrange
        LocalDate releaseDate = LocalDate.now();
        List<Movie> movieList = Arrays.asList(new Movie(), new Movie());
        when(movieRepository.findByReleaseDate(releaseDate)).thenReturn(movieList);

        // Act
        List<Movie> result = movieController.getMoviesByReleaseDate(releaseDate);

        // Assert
        assertEquals(movieList, result);
    }
}
