package com.example.movieapp;

import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Repository
public interface MovieRepository extends CassandraRepository<Movie, UUID> {
    @AllowFiltering
    List<Movie> findByTitle(String title);
    @AllowFiltering
    List<Movie> findByReleaseDate(LocalDate releaseDate);
}
