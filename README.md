# Movie Application

The Movie Application is a Spring Boot application that allows users to manage and search for movies, actors, and directors in a Cassandra database. This README provides information on how to set up and run the application.

## Prerequisites

Before you begin, ensure you have met the following requirements:

- Java Development Kit (JDK)
- Apache Cassandra (with a running instance)
- Maven or Gradle (depending on your preference)
- Git (optional for cloning the repository)

## How to Run

1. **Database Configuration**:
    - Create a Cassandra keyspace named `social_db`.
    - Update the database credentials in `src/main/resources/application.properties` file if needed.

2. **Build and Run**:
    - Navigate to the project directory using the terminal.
    - Use the following Maven command to build and run the application:

   ```bash
   mvn spring-boot:run


# Feedback

- Was it easy to complete the task using AI? *Actually no, because ChatGPT has limited knowledge which is deprecated nowadays.*
- How long did the task take you to complete? *Around 1-1.5 hours.*
- Was the code ready to run after generation? What did you have to change to make it usable? *The generated content provided a good starting point but required customization.*
- Which challenges did you face during completion of the task? *Covering 100% of code with unit tests.*
- Which specific prompts you learned as a good practice to complete the task? *Sometimes need googling to search for some new information.* 

